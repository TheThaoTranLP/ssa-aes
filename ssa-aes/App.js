import React from 'react';

import {
  Text,
  Image,
  Animated,
  StatusBar,
  Easing,
} from 'react-native';
import Svg, { G, Ellipse } from 'react-native-svg';
import {
  createStackNavigator,
  createBottomTabNavigator,
} from 'react-navigation';
import { Ionicons } from '@expo/vector-icons';
import {
  Font,
  Asset,
  SplashScreen,
} from 'expo';
import Amplify from 'aws-amplify';
import PropTypes from 'prop-types';

import { LoginScreen } from './views/Login';
import HomeScreen from './views/Home';
import { FAQScreen } from './views/FAQ';
import { ScheduleScreen } from './views/Schedule';
import { EventDetailsScreen } from './views/EventDetails';
import { ConnectScreen } from './views/Connect';
import { HomeIcon } from './views/HomeIcon';
import Constants from './views/components/Constants';
import { Database, rebuildLocalDB } from './views/components/Database';
import awsExports from './aws-exports';

const { styles, ssaBlue, powerRed } = Constants;
const AnimatedEllipse = Animated.createAnimatedComponent(Ellipse);
const AnimatedSvg = Animated.createAnimatedComponent(Svg);
const AnimatedG = Animated.createAnimatedComponent(G);
const AnimatedStatusBar = Animated.createAnimatedComponent(StatusBar);

const ssaLogoBlue = require('./assets/img/ssa-logo-blue-no-bg.png');
const ssaLogoGrey = require('./assets/img/ssa-logo-grey-no-bg.png');
const bebasNeueRegular = require('./assets/fonts/BebasNeue-Regular.otf');
const avenirLight = require('./assets/fonts/Avenir/Avenir-Light.ttf');
const avenirMedium = require('./assets/fonts/Avenir/Avenir-Medium.ttf');

Amplify.configure(awsExports);

const ScheduleStack = createStackNavigator(
  {
    Overview: ScheduleScreen,
    EventDetails: EventDetailsScreen,
  },
  {
    initialRouteName: 'Overview',
    mode: 'modal',
    navigationOptions: {
      header: null,
    },
  },
);

function NavigationOptions({ navigation }) {
  function TabBarIcon({ focused, tintColor }) {
    const { routeName } = navigation.state;
    let iconName;
    if (routeName === 'Home') {
      return <HomeIcon focused={focused} />;
    }

    if (routeName === 'Schedule') {
      iconName = 'md-calendar';
    } else if (routeName === 'FAQ') {
      iconName = 'md-information-circle';
    } else if (routeName === 'Connect') {
      iconName = 'md-contact';
    }

    return <Ionicons name={iconName} size={30} color={tintColor} />;
  }

  TabBarIcon.propTypes = {
    focused: PropTypes.bool.isRequired,
    tintColor: PropTypes.string.isRequired,
  };

  return {
    tabBarIcon: TabBarIcon,
  };
}

const TabBar = createBottomTabNavigator(
  {
    Home: HomeScreen,
    Schedule: ScheduleStack,
    FAQ: FAQScreen,
    Connect: ConnectScreen,
  },
  {
    initialRouteName: 'Home',
    navigationOptions: NavigationOptions,
    tabBarOptions: {
      activeTintColor: Constants.greyedOut,
      inactiveTintColor: ssaBlue,
      activeBackgroundColor: 'white',
      inactiveBackgroundColor: 'white',
      showLabel: false,
      style: {
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
        borderTopColor: 'transparent',
      },
    },
  },
);

const transitionConfig = () => ({
  transitionSpec: {
    duration: 2000,
    easing: Easing.out(Easing.poly(4)),
    timing: Animated.timing,
    useNativeDriver: true,
  },
  screenInterpolator: (sceneProps) => {
    const { position, scene } = sceneProps;

    const thisSceneIndex = scene.index;

    const opacity = position.interpolate({
      inputRange: [thisSceneIndex - 1, thisSceneIndex],
      outputRange: [0, 1],
    });

    return { opacity };
  },
});

const AppNav = createStackNavigator(
  {
    Login: LoginScreen,
    App: TabBar,
  },
  {
    initialRouteName: 'Login',
    navigationOptions: {
      header: null,
    },
    transitionConfig,
  },
);

function cacheImages(images) {
  return images.map((image) => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    }
    return Asset.fromModule(image).downloadAsync();
  });
}

function cacheFonts(fonts) {
  return fonts.map(font => Font.loadAsync(font));
}

async function loadAssetsAsync() {
  const imageAssets = cacheImages([
    ssaLogoBlue,
    ssaLogoGrey,
  ]);

  const fontAssets = cacheFonts([{
    'bebas-neue-regular': bebasNeueRegular,
    'avenir-light': avenirLight,
    'avenir-medium': avenirMedium,
  }, Ionicons.font]);

  await Promise.all([...imageAssets, ...fontAssets]);
}

export default class App extends React.Component {
  state = {
    isReady: false,
    loggedIn: false,
    loadingOpacity: 0,
    loadingStyle: {},
    atomAnim1: new Animated.Value(0),
    atomAnim2: new Animated.Value(775),
    fadeAnim: new Animated.Value(0),
    screenFade: new Animated.Value(1),
    scaleAnim: new Animated.Value(0.75),
    unravelAnim: new Animated.Value(0),
    atomAtBegin: false,
    svgSize: 500 * 0.75,
  };

  async componentDidMount() {
    const {
      atomAnim1,
      atomAnim2,
      scaleAnim,
      unravelAnim,
      screenFade,
    } = this.state;

    SplashScreen.preventAutoHide();

    atomAnim1.addListener(({ value }) => {
      if (Math.round(value) === 950) {
        this.setState({ atomAtBegin: true });
      } else {
        this.setState({ atomAtBegin: false });
      }
    });

    scaleAnim.addListener(({ value }) => {
      if (Math.round(value * 100) / 100 === 0.5) {
        Animated.timing(
          unravelAnim,
          {
            toValue: 1,
          },
        ).start();
      }
      this.setState({ svgSize: 500 * value });
    });

    unravelAnim.addListener(({ value }) => {
      if (Math.round(value * 100) / 100 === 1) {
        this.setState({ isReady: true });
      }
    });

    Animated.loop(
      Animated.timing(
        atomAnim1,
        {
          toValue: 950,
          duration: 2000,
        },
      ),
      {
        iterations: -1,
      },
    ).start();
    Animated.loop(
      Animated.timing(
        atomAnim2,
        {
          toValue: 1725,
          duration: 2000,
        },
      ),
      {
        iterations: -1,
      },
    ).start();

    await loadAssetsAsync();

    this.setLoadingTimer();

    await rebuildLocalDB();

    Database.transaction(
      (tx) => {
        tx.executeSql(
          'select * from credentials;',
          [],
          (_, { rows: { _array } }) => {
            if (_array.length === 0) {
              Animated.timing(
                scaleAnim,
                {
                  toValue: 0.5,
                  duration: 1500,
                },
              ).start();
              this.setState({ loadingOpacity: 0 });
            } else {
              this.setState({ loadingOpacity: 0 });
              Animated.timing(
                screenFade,
                {
                  toValue: 0,
                  duration: 2000,
                },
              ).start();
              screenFade.addListener(({ value }) => {
                if (Math.round(value * 100) / 100 === 0) {
                  this.setState({ loggedIn: true, isReady: true });
                }
              });
            }
          },
        );
      },
    );
  }

  async setLoadingTimer() {
    setTimeout(
      () => {
        this.setState({
          loadingOpacity: 1,
          loadingStyle: [
            styles.calendarDate,
            {
              color: 'white',
              width: '75%',
              textAlign: 'center',
              textAlignVertical: 'top',
            },
          ],
        });
      },
      5000,
    );
  }

  render() {
    const {
      isReady,
      atomAtBegin,
      screenFade,
      fadeAnim,
      scaleAnim,
      atomAnim1,
      atomAnim2,
      unravelAnim,
      loadingOpacity,
      loadingStyle,
      loggedIn,
      svgSize,
    } = this.state;

    if (!isReady || !atomAtBegin) {
      return (
        <Animated.View style={{ flex: 1, backgroundColor: ssaBlue, opacity: screenFade }}>
          <Animated.View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              opacity: fadeAnim,
            }}
          >
            <AnimatedStatusBar
              barStyle="light-content"
              backgroundColor={
                screenFade.interpolate({
                  inputRange: [0, 1],
                  outputRange: ['white', ssaBlue],
                })
              }
            />
            <Image
              onLoadEnd={() => {
                Animated.timing(
                  fadeAnim,
                  {
                    toValue: 1,
                    duration: 2000,
                  },
                ).start();
                SplashScreen.hide();
              }}
            />
            <Animated.View
              style={{
                height: scaleAnim.interpolate({
                  inputRange: [0.5, 0.75],
                  outputRange: ['0%', '20%'],
                }),
              }}
            >
            </Animated.View>
            <AnimatedSvg height={svgSize} width={svgSize}>
              <AnimatedG scale={scaleAnim}>
                <AnimatedEllipse
                  cx="239.275888"
                  cy="224.642723"
                  fill="none"
                  rx="75.206059"
                  ry="208.474597"
                  stroke="#ffffff"
                  strokeWidth="15"
                  transform="rotate(33 239.276 224.643)"
                  strokeLinecap="round"
                  strokeDasharray="600, 350"
                  strokeDashoffset={atomAnim2}
                />
                <AnimatedEllipse
                  cx="242.94001"
                  cy="227.744106"
                  fill="none"
                  rx="76.212462"
                  ry="207.703417"
                  stroke="#ffffff"
                  strokeWidth="15"
                  transform="rotate(-26.5 242.94 227.744)"
                  strokeLinecap="round"
                  strokeDasharray="775, 175"
                  strokeDashoffset={atomAnim1}
                />
                <AnimatedEllipse
                  cx="244.075888"
                  cy="224.642722"
                  fill="none"
                  rx="75.206059"
                  ry="208.474597"
                  stroke="#ffffff"
                  strokeWidth="15"
                  transform="rotate(93 244.076 224.643)"
                  strokeLinecap="round"
                  strokeDasharray="600, 350"
                  strokeDashoffset={atomAnim2}
                />
                <AnimatedEllipse
                  cx="241.200002"
                  cy="221.200002"
                  fill="#ffffff"
                  rx="25.599999"
                  ry="25.599999"
                />
              </AnimatedG>
            </AnimatedSvg>
            <Animated.View
              style={{
                backgroundColor: 'white',
                height: scaleAnim.interpolate({
                  inputRange: [0.5, 0.75],
                  outputRange: [40, 0],
                }),
                width: '75%',
                borderRadius: 10,
                scaleY: unravelAnim,
              }}
            >
            </Animated.View>
            <Animated.View
              style={{
                height: scaleAnim.interpolate({
                  inputRange: [0.5, 0.75],
                  outputRange: [10, 0],
                }),
              }}
            >
            </Animated.View>
            <Animated.View
              style={{
                backgroundColor: 'white',
                height: scaleAnim.interpolate({
                  inputRange: [0.5, 0.75],
                  outputRange: [40, 0],
                }),
                width: '75%',
                borderRadius: 10,
                scaleY: unravelAnim,
              }}
            >
            </Animated.View>
            <Animated.View
              style={{
                height: scaleAnim.interpolate({
                  inputRange: [0.5, 0.75],
                  outputRange: [10, 0],
                }),
              }}
            >
            </Animated.View>
            <Animated.View
              style={{
                backgroundColor: 'white',
                height: scaleAnim.interpolate({
                  inputRange: [0.5, 0.75],
                  outputRange: [40, 0],
                }),
                width: '75%',
                borderRadius: 10,
                scaleY: unravelAnim,
              }}
            >
            </Animated.View>
            <Animated.View
              style={{
                height: scaleAnim.interpolate({
                  inputRange: [0.5, 0.75],
                  outputRange: [10, 0],
                }),
              }}
            >
            </Animated.View>
            <Animated.View
              style={{
                backgroundColor: powerRed,
                height: scaleAnim.interpolate({
                  inputRange: [0.5, 0.75],
                  outputRange: [40, 0],
                }),
                width: 100,
                borderRadius: 10,
                scaleY: unravelAnim,
              }}
            >
            </Animated.View>
            <Animated.View
              style={{
                height: scaleAnim.interpolate({
                  inputRange: [0.5, 0.75],
                  outputRange: [10, 0],
                }),
              }}
            >
            </Animated.View>
            <Animated.View
              style={{
                height: scaleAnim.interpolate({
                  inputRange: [0.5, 0.75],
                  outputRange: [30, 0],
                }),
              }}
            >
            </Animated.View>
            <Animated.View
              style={{
                height: scaleAnim.interpolate({
                  inputRange: [0.5, 0.75],
                  outputRange: [10, 0],
                }),
              }}
            >
            </Animated.View>
            <Animated.Text
              style={{
                height: scaleAnim.interpolate({
                  inputRange: [0.5, 0.75],
                  outputRange: [60, 0],
                }),
              }}
            >
            </Animated.Text>
          </Animated.View>
          <Animated.View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'flex-start',
              opacity: loadingOpacity,
              height: scaleAnim.interpolate({
                inputRange: [0.5, 0.75],
                outputRange: ['0%', '20%'],
              }),
            }}
          >
            <Text style={loadingStyle}>hold on a sec</Text>
          </Animated.View>
        </Animated.View>
      );
    }

    if (loggedIn) {
      return <TabBar />;
    }

    return <AppNav />;
  }
}
