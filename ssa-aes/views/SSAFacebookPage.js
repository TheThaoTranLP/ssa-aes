import React from 'react';
import {
  WebView,
  Linking,
  Animated,
} from 'react-native';


const ssaPage = require('./components/ssa-aes-page.html');

const AnimatedWebView = Animated.createAnimatedComponent(WebView);

export class SSAFacebookPage extends React.Component {
  state = {
    loaded: false,
    opacityAnim: new Animated.Value(0),
  };

  openLink(event) {
    const { loaded } = this.state;
    if (loaded) {
      this.webview.stopLoading();
      Linking.openURL(event.url);
    }
  }

  render() {
    const { opacityAnim } = this.state;

    return (
      <AnimatedWebView
        ref={(ref) => { this.webview = ref; }}
        source={ssaPage}
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          opacity: opacityAnim,
        }}
        onLoadEnd={() => {
          this.setState({ loaded: true });
          Animated.timing(
            opacityAnim,
            {
              toValue: 1,
              duration: 4000,
            },
          ).start();
        }}
        onNavigationStateChange={(event) => { this.openLink(event); }}
      />
    );
  }
}
