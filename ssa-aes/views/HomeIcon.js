import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'react-native';

const ssaLogoBlue = require('../assets/img/ssa-logo-blue-no-bg.png');
const ssaLogoGrey = require('../assets/img/ssa-logo-grey-no-bg.png');

export class HomeIcon extends React.Component {
  render() {
    const { focused } = this.props;
    let icon;
    if (!focused) {
      icon = (
        <Image
          source={ssaLogoBlue}
          resizeMode="contain"
          style={{ flex: 1, maxHeight: 30 }}
        />
      );
    } else {
      icon = (
        <Image
          source={ssaLogoGrey}
          resizeMode="contain"
          style={{ flex: 1, maxHeight: 30 }}
        />
      );
    }

    return icon;
  }
}

HomeIcon.propTypes = {
  focused: PropTypes.bool.isRequired,
};
