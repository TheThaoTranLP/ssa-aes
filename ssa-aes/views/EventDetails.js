import React from 'react';
import {
  Linking,
  Text,
  View,
  ScrollView,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Image,
  StatusBar,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import { Ionicons } from '@expo/vector-icons';

import Constants from './components/Constants';
import { Database } from './components/Database';

const { styles, month, day } = Constants;
const mapRequestCenter = 'https://maps.googleapis.com/maps/api/staticmap?center=';
const mapRequestParams = '&zoom=17&size=300x300&format=png&maptype=roadmap&markers=color:red%7C';
const mapRequestKey = '&key=AIzaSyATBO9paq_ByUOqtlj6TybfoV0WMdWk5gI';

export class EventDetailsScreen extends React.Component {
  constructor() {
    super();
    this.state = { saved: false };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.getFavourite(navigation.getParam('currentEvent', null));
  }

  getFavourite = (currentEvent) => {
    if (currentEvent == null) {
      console.log('Error getting event data from database: received null event object');
      return;
    }
    Database.transaction(
      (tx) => {
        tx.executeSql(
          'select saved from events where id=?;',
          [currentEvent.id],
          (_, { rows: { _array } }) => {
            let dbFavourite = false;
            if (_array.length > 0) {
              dbFavourite = _array[0].saved === 1;
            }
            this.setState({ saved: dbFavourite });
          },
          (error) => { console.log(error); },
        );
      },
      (error) => { console.log(error); },
    );
  }

  setFavourite = (currentEvent) => {
    const { saved } = this.state;
    Database.transaction(
      (tx) => {
        tx.executeSql(
          'update events set saved=? where id=?;',
          [!saved ? 1 : 0, currentEvent.id],
        );
      },
      (error) => { console.log(error); },
      () => { this.getFavourite(currentEvent); },
    );
  }

  getEventTime = (currentEvent) => {
    const start = new Date(currentEvent.start.dateTime);
    const end = new Date(currentEvent.end.dateTime);

    let startHour = start.getHours();
    const startMaybeMidnight = startHour === 0 ? 12 : startHour;
    const endMaybeMidnight = end.getHours() === 0 ? 12 : end.getHours();
    startHour = startHour > 12 ? startHour - 12 : startMaybeMidnight;
    const startMinute = start.getMinutes() > 0 ? `:${start.getMinutes()}` : '';
    const startPeriod = startHour > 11 ? 'p.m.' : 'a.m.';
    const endHour = end.getHours() > 12 ? end.getHours() - 11 : endMaybeMidnight;
    const endMinute = end.getMinutes() > 0 ? `:${end.getMinutes()}` : '';
    const endPeriod = end.getHours() > 11 ? 'p.m.' : 'a.m.';

    const startTime = `${startHour}${startMinute} ${startPeriod}`;
    const endTime = `${endHour}${endMinute} ${endPeriod}`;

    return `${day[start.getDay()]}, ${month[start.getMonth()]} ${start.getDate()}, ${startTime} - ${day[end.getDay()]}, ${month[end.getMonth()]} ${end.getDate()}, ${endTime}`;
  }

  getEventContentComponent = (content, iconName) => (
    <View
      style={{
        flex: 1, flexDirection: 'row', alignItems: 'flex-start', marginTop: 10,
      }}
    >
      <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
        <Ionicons name={iconName} size={25} color={Constants.ssaBlue} />
      </View>
      <View style={{ flex: 8, flexDirection: 'row', justifyContent: 'flex-start' }}>
        <Text
          style={[styles.eventOverview, {
            color: '#333', textAlign: 'left', textAlignVertical: 'center', fontSize: 16, marginTop: 3,
          }]}
        >
          {content}
        </Text>
      </View>
    </View>
  );

  render() {
    const { navigation } = this.props;
    const { saved } = this.state;
    const currentEvent = navigation.getParam('currentEvent', null);
    const returnRoute = navigation.getParam('returnRoute', null);
    const staticMapRequest = `${mapRequestCenter}${currentEvent.location}${mapRequestParams}${currentEvent.location}${mapRequestKey}`;
    const mapLink = `https://www.google.com/maps/search/?api=1&query=${currentEvent.location}`;

    return (
      <View style={[styles.background, { flexDirection: 'column', flex: 1, justifyContent: 'flex-start' }]}>
        <StatusBar barStyle="dark-content" backgroundColor="white" />
        {
          Platform.OS === 'ios' ? <View style={{ marginTop: 20 }} /> : null
        }
        <View
          style={{
            marginTop: 10,
            marginLeft: 20,
            marginRight: 20,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}
        >
          <TouchableOpacity
            onPress={() => navigation.navigate(returnRoute)}
            hitSlop={{
              top: 10, bottom: 10, left: 10, right: 10,
            }}
          >
            <Ionicons name="md-close" size={40} color="#333" />
          </TouchableOpacity>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'flex-end',
              alignItems: 'center',
            }}
          >
            <TouchableWithoutFeedback
              onPress={() => { this.setFavourite(currentEvent); }}
              hitSlop={{
                top: 10, bottom: 10, left: 10, right: 10,
              }}
            >
              <Ionicons name={saved ? 'md-star' : 'md-star-outline'} size={40} color={saved ? 'yellow' : '#333'} />
            </TouchableWithoutFeedback>
          </View>
        </View>
        <ScrollView style={{ flex: 20 }}>
          <View style={[styles.content, { marginTop: 10 }]}>
            <Text
              style={[styles.title, { fontSize: 40, marginBottom: 10 }]}
            >
              {currentEvent.summary}
            </Text>
            {
              this.getEventContentComponent(this.getEventTime(currentEvent), 'md-time')
            }
            {
              currentEvent.description ? this.getEventContentComponent(currentEvent.description, 'md-list') : null
            }
            {
              currentEvent.location ? (
                <View style={{ flexDirection: 'column', flex: 1 }}>
                  { this.getEventContentComponent(currentEvent.location, 'md-pin') }
                  <View
                    style={{
                      marginTop: 20,
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      flex: 1,
                    }}
                  >
                    <TouchableOpacity onPress={() => { Linking.openURL(mapLink); }}>
                      <Image
                        style={{ width: 300, height: 300 }}
                        source={{ uri: staticMapRequest }}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              ) : null
            }
          </View>
        </ScrollView>
      </View>
    );
  }
}

EventDetailsScreen.propTypes = {
  navigation: PropTypes.shape({
    getParam: PropTypes.func.isRequired,
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};
