import React from 'react';
import {
  RefreshControl,
  Text,
  View,
  ScrollView,
  StatusBar,
  Platform,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {
  NavigationEvents,
} from 'react-navigation';
import { Ionicons } from '@expo/vector-icons';
import PropTypes from 'prop-types';
import Constants from './components/Constants';
import { Database, getEvents } from './components/Database';
import { Event } from './Event';

const { styles, month: months, day: days } = Constants;

const Day = (props) => {
  const {
    events,
    datetime,
    navigation,
    month,
    day,
    date,
  } = props;

  const dayEvents = events[datetime].map(event => (
    <Event key={event.id} event={event} events={events} navigation={navigation} />
  ));
  return (
    <View style={{ flex: 1, flexDirection: 'row', marginBottom: 20 }}>
      <View style={{
        flex: 1, flexDirection: 'row', justifyContent: 'center',
      }}
      >
        <Text style={{ textAlign: 'center', textAlignVertical: 'top' }}>
          <Text style={[styles.calendarDate, { fontSize: 14 }]}>
            { `${month}\n` }
          </Text>
          <Text style={styles.calendarDate}>
            { `${date}\n` }
          </Text>
          <Text style={[styles.calendarDate, { fontSize: 14 }]}>
            { `${day}\n` }
          </Text>
        </Text>
      </View>
      <View style={{ flex: 9, flexDirection: 'column', justifyContent: 'flex-start' }}>
        {dayEvents}
      </View>
    </View>
  );
};

Day.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    state: PropTypes.shape({
      routeName: PropTypes.string,
    }),
  }).isRequired,
  events: PropTypes.shape({}).isRequired,
  datetime: PropTypes.string.isRequired,
  month: PropTypes.string.isRequired,
  day: PropTypes.string.isRequired,
  date: PropTypes.number.isRequired,
};

export class ScheduleScreen extends React.Component {
  constructor() {
    super();
    this.calendarByDate = {};
    this.dayEvents = [];
    this.state = {
      loaded: false,
      refreshing: false,
      favourites: false,
    };
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    getEvents();
    this.setState({ refreshing: false });
  }

  parseCalendar = (sortedEvents) => {
    const { navigation } = this.props;
    const { favourites, refreshing } = this.state;
    let dayEvents = [];

    this.calendarByDate = {};
    try {
      for (let i = 0; i < sortedEvents.length; i += 1) {
        const event = sortedEvents[i];
        const eventDatetime = new Date(event.start.dateTime);
        const eventDay = new Date(
          eventDatetime.getFullYear(),
          eventDatetime.getMonth(),
          eventDatetime.getDate(),
        );

        if (!(eventDay in this.calendarByDate)) {
          this.calendarByDate[eventDay] = [];
        }

        this.calendarByDate[eventDay] = this.calendarByDate[eventDay].concat(event);
      }

      dayEvents = Object.keys(this.calendarByDate)
        .map(
          date => (
            <Day
              key={date}
              datetime={date}
              month={months[(new Date(date)).getMonth()]}
              date={(new Date(date)).getDate()}
              day={days[(new Date(date)).getDay()]}
              events={this.calendarByDate}
              navigation={navigation}
            />
          ),
        );

      if (dayEvents.length === 0) {
        let emptyEventsText = '';
        if (!favourites) {
          emptyEventsText = 'Events are being planned. Stay tuned!';
        } else {
          emptyEventsText = 'Save events from the schedule to see them here!';
        }
        this.dayEvents = (
          <View
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              flex: 1,
            }}
          >
            <Text style={[styles.body, { textAlign: 'center' }]}>{emptyEventsText}</Text>
          </View>
        );
      } else {
        this.dayEvents = (
          <ScrollView
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={this._onRefresh} />
            }
          >
            {
              dayEvents
            }
          </ScrollView>
        );
      }

      this.setState({ refreshing: false, loaded: true });
    } catch (error) {
      console.log(error);
    }
  }

  getEventsFromDatabase = () => {
    const { favourites } = this.state;
    let sortedEvents = [];
    Database.transaction((tx) => {
      let sqlStatement = 'select * from events';
      if (favourites) {
        sqlStatement += ' where saved=1';
      }
      sqlStatement += ';';
      tx.executeSql(
        `${sqlStatement}`,
        [],
        (_, { rows: { _array } }) => {
          sortedEvents = _array
            .map(element => JSON.parse(element.event))
            .sort((a, b) => (
              (new Date(a.start.dateTime)).getTime() - (new Date(b.start.dateTime)).getTime()
            ));
          this.parseCalendar(sortedEvents);
        },
        null,
        (_, error) => { console.log(error); },
      );
    },
    (error) => { console.log(error); });
  };

  toggleFavourites = () => {
    this.setState(prevState => ({
      favourites: !prevState.favourites,
    }), this.getEventsFromDatabase);
  }

  ScheduleHeader = () => {
    const { favourites } = this.state;
    return (
      <View style={styles.header}>
        <View style={styles.headerElement} />
        <View style={styles.headerElement}>
          <Text style={[styles.headerText, { fontSize: 40 }]}>Schedule</Text>
        </View>
        <View style={styles.headerElement}>
          <TouchableOpacity
            onPress={() => { this.toggleFavourites(); }}
            hitSlop={{
              top: 10, bottom: 10, left: 10, right: 10,
            }}
          >
            <Ionicons name={favourites ? 'md-star' : 'md-star-outline'} size={40} color={favourites ? 'yellow' : '#333'} />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  render() {
    const { loaded } = this.state;
    return (
      <View style={[styles.background, styles.content, { margin: 0 }]}>
        <NavigationEvents onWillFocus={this.getEventsFromDatabase} />
        <StatusBar barStyle="dark-content" backgroundColor="white" />
        {
          Platform.OS === 'ios'
            ? <View style={{ marginTop: 20 }} /> : null
        }
        { this.ScheduleHeader() }
        {
          loaded ? this.dayEvents
            : <ActivityIndicator size="large" color={Constants.ssaBlue} />
        }
      </View>
    );
  }
}

ScheduleScreen.propTypes = {
  navigation: PropTypes.shape({
    state: PropTypes.shape({
      routeName: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};
