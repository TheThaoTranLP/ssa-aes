import React from 'react';
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  StatusBar,
  ActivityIndicator,
  Platform,
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import { Localization } from 'expo-localization';
import Constants from './components/Constants';
import { Database } from './components/Database';
import { Question } from './Question';

const { styles } = Constants;

export class FAQScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      isReady: false,
      faqQuestions: null,
      language: 'english',
      enStyle: [styles.headerText, { color: Constants.greyedOut }],
      frStyle: styles.headerText,
    };
  }

  componentDidMount() {
    const { language } = this.state;
    const { locale } = Localization;
    if (locale.includes('fr') && language === 'english') {
      this.changeLanguage('english');
    } else if (locale.includes('en') && language === 'french') {
      this.changeLanguage('english');
    }
  }

  changeLanguage = (language) => {
    if (language === 'english') {
      this.setState({
        language: 'english',
        enStyle: [styles.headerText, { color: Constants.greyedOut }],
        frStyle: styles.headerText,
      });
    } else {
      this.setState({
        language: 'french',
        enStyle: styles.headerText,
        frStyle: [styles.headerText, { color: Constants.greyedOut }],
      });
    }
  };

  refreshFAQ = (language) => {
    Database.transaction(
      (tx) => {
        tx.executeSql(
          'select * from faq where language=?;',
          [language],
          (_, { rows: { _array } }) => {
            const faqQuestions = [];
            _array.forEach((row) => {
              faqQuestions.push(
                <Question
                  key={row.question}
                  question={row.question}
                  answer={row.answer}
                />,
              );
            });
            this.setState({
              faqQuestions,
              isReady: true,
            });
          },
          (_, error) => { console.log(error); },
        );
      },
      (error) => { console.log(error); },
    );
  }

  faqHeader = () => {
    const { language, enStyle, frStyle } = this.state;

    return (
      <View style={styles.header}>
        <View style={styles.headerElement}>
          <TouchableOpacity
            onPress={() => {
              if (language === 'french') {
                this.changeLanguage('english');
                this.refreshFAQ('english');
              }
            }}
            hitSlop={{
              top: 10, bottom: 10, left: 10, right: 10,
            }}
          >
            <Text style={enStyle}>EN</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.headerElement}>
          <Text style={[styles.headerText, { fontSize: 40 }]}>FAQ</Text>
        </View>
        <View style={styles.headerElement}>
          <TouchableOpacity
            onPress={() => {
              if (language === 'english') {
                this.changeLanguage('french');
                this.refreshFAQ('french');
              }
            }}
            hitSlop={{
              top: 10, bottom: 10, left: 10, right: 10,
            }}
          >
            <Text style={frStyle}>FR</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  render() {
    const { isReady, faqQuestions, language } = this.state;
    if (!isReady) {
      return (
        <View style={styles.background}>
          <NavigationEvents
            onWillFocus={() => this.refreshFAQ(language)}
          />
          <StatusBar barStyle="dark-content" backgroundColor="white" />
          {
            Platform.OS === 'ios'
              ? <View style={{ marginTop: 20 }} /> : null
          }
          { this.faqHeader() }
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <ActivityIndicator size="large" color={Constants.ssaBlue} />
          </View>
        </View>
      );
    }
    return (
      <View style={styles.background}>
        <NavigationEvents
          onWillFocus={() => this.refreshFAQ(language)}
        />
        <StatusBar barStyle="dark-content" backgroundColor="white" />
        {
          Platform.OS === 'ios'
            ? <View style={{ marginTop: 20 }} /> : null
        }
        { this.faqHeader() }
        <View style={[styles.content, { marginBottom: 0, marginTop: 0 }]}>
          <ScrollView>
            { faqQuestions }
          </ScrollView>
        </View>
      </View>
    );
  }
}
