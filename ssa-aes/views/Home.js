import React from 'react';
import {
  Text,
  View,
  Image,
  StatusBar,
  Platform,
} from 'react-native';

import Constants from './components/Constants';
import { SSAFacebookPage } from './SSAFacebookPage';

const { styles } = Constants;
const ssaLogo = require('../assets/img/ssa-logo-blue-no-bg.png');

const HomeHeader = () => (
  <View style={styles.header}>
    <View style={styles.headerElement}>
      <Text style={styles.headerText}> uOttawa </Text>
    </View>
    <View style={styles.headerElement}>
      <Image
        source={ssaLogo}
        resizeMode="contain"
        style={{ flex: 1, maxHeight: 40 }}
      />
    </View>
    <View style={styles.headerElement}>
      <Text style={styles.headerText}> SSA-AES </Text>
    </View>
  </View>
);

const HomeScreen = () => (
  <View style={styles.background}>
    <StatusBar barStyle="dark-content" backgroundColor="white" />
    {
      Platform.OS === 'ios'
        ? <View style={{ marginTop: 20 }} /> : null
    }
    {
      HomeHeader()
    }
    <View style={[styles.content, { flex: 1 }]}>
      <SSAFacebookPage />
    </View>
  </View>
);

export default HomeScreen;
