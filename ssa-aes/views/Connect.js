import React from 'react';
import {
  Text,
  View,
  Image,
  StatusBar,
  TouchableOpacity,
  Linking,
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import { Ionicons } from '@expo/vector-icons';

import Constants from './components/Constants';
import { Database } from './components/Database';

const { styles } = Constants;
const { ssaBlue } = Constants;
const ssaLogo = require('../assets/img/ssa-logo-blue-no-bg.png');

export class ConnectScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      website: null,
      socialMedia: [],
      phone: null,
    };
  }

  componentDidMount() {
    this.getSocialMedia();
  }

  getSocialMedia = () => {
    Database.transaction(
      (tx) => {
        tx.executeSql(
          'select * from socialmedia',
          [],
          (_, { rows: { _array } }) => {
            const socialMedia = [];
            _array.forEach((row) => {
              if (row.name === 'website') {
                this.setState({
                  website: (
                    <TouchableOpacity
                      onPress={() => { Linking.openURL(row.value); }}
                      hitSlop={{
                        top: 10, bottom: 10, left: 10, right: 10,
                      }}
                      style={{ marginTop: 20, marginBottom: 20 }}
                    >
                      <Text style={styles.subtitle}>{ row.value.replace(/(.+\/\/)|(\W$)/g, '') }</Text>
                    </TouchableOpacity>
                  ),
                });
              } else if (row.name === 'phone') {
                const phoneNumber = row.value.replace(/(\D)|(\s)|(\sext.*)|(\d*$)/g, '');
                this.setState({
                  phone: (
                    <TouchableOpacity
                      onPress={() => { Linking.openURL(`tel:${phoneNumber}`); }}
                      hitSlop={{
                        top: 10, bottom: 10, left: 10, right: 10,
                      }}
                      style={{ marginTop: 20 }}
                    >
                      <Text style={styles.body}>{ row.value }</Text>
                    </TouchableOpacity>
                  ),
                });
              } else {
                let icon = `logo-${row.name}`;
                let url = row.value;

                if (row.name === 'email') {
                  icon = 'md-mail';
                  url = `mailto:${url}`;
                } else if (row.name === 'locationName') {
                  icon = 'md-pin';
                  url = `https://www.google.com/maps/search/?api=1&query=${row.value}`;
                }

                socialMedia.push((
                  <TouchableOpacity
                    key={icon}
                    onPress={() => { Linking.openURL(url); }}
                    hitSlop={{
                      top: 10, bottom: 10, left: 10, right: 10,
                    }}
                    style={{ marginLeft: 10, marginRight: 10 }}
                  >
                    <Ionicons name={icon} size={30} color={ssaBlue} />
                  </TouchableOpacity>
                ));
              }
            });

            this.setState({ socialMedia });
          },
          (_, error) => {
            console.log('error getting social media');
            console.log(error);
          },
        );
      },
      (error) => { console.log(error); },
    );
  }

  render() {
    const { website, socialMedia, phone } = this.state;

    return (
      <View style={[styles.background]}>
        <NavigationEvents onWillFocus={this.getSocialMedia} />
        <StatusBar barStyle="dark-content" backgroundColor="white" />
        <View style={{
          flexDirection: 'column', flex: 1, justifyContent: 'center', alignItems: 'center', marginBottom: 60,
        }}
        >
          <Image source={ssaLogo} resizeMode="contain" style={{ flex: 1, maxHeight: '30%', marginTop: '30%' }} />
          { website }
          <View style={{ flexDirection: 'row', justifyItems: 'center', alignItems: 'center' }}>
            { socialMedia }
          </View>
          { phone }
        </View>
      </View>
    );
  }
}
