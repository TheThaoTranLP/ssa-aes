import React from 'react';
import {
  Text,
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';
import { Ionicons } from '@expo/vector-icons';
import Constants from './components/Constants';

const { styles } = Constants;

export class Question extends React.Component {
  constructor() {
    super();
    this.state = {
      isHidden: true,
      arrow: 'ios-arrow-forward',
    };
  }

  toggleStatus() {
    let { isHidden } = this.state;
    isHidden = !isHidden;
    const arrow = isHidden ? 'ios-arrow-forward' : 'ios-arrow-down';
    this.setState({ isHidden, arrow });
  }

  render() {
    const { arrow, isHidden } = this.state;
    const { question, answer } = this.props;

    return (
      <View style={styles.background}>
        <TouchableWithoutFeedback onPress={() => this.toggleStatus()}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              alignItems: 'flex-start',
              justifyContent: 'flex-start',
            }}
          >
            <Ionicons style={{ flex: 1, maxWidth: 25, marginTop: 5 }} name={arrow} size={25} color="#333" />
            <Text style={[{ flex: 1, margin: 0 }, styles.question]}>{ question }</Text>
          </View>
        </TouchableWithoutFeedback>
        {
          isHidden ? null
            : <Text style={[styles.body, { flex: 1, marginLeft: 25 }]}>{ answer }</Text>
        }
      </View>
    );
  }
}

Question.propTypes = {
  question: PropTypes.string.isRequired,
  answer: PropTypes.string.isRequired,
};
