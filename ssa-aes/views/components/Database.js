import { SQLite } from 'expo';
import { API } from 'aws-amplify';

export const Database = SQLite.openDatabase('ssa-aes.db');

async function createTable(tableName, columns, types, asyncGet) {
  let columnSQL = '';
  for (let col = 0; col < columns.length; col += 1) {
    if (columnSQL !== '') {
      columnSQL += ', ';
    }

    let primaryKey = '';
    if (col === 0) {
      primaryKey = ' primary key not null';
    }

    columnSQL += `${columns[col]} ${types[col]}${primaryKey}`;
  }

  Database.transaction((tx) => {
    tx.executeSql(`drop table ${tableName};`);
  });

  return new Promise((resolve, reject) => {
    Database.transaction(
      (tx) => {
        tx.executeSql(
          `create table if not exists ${tableName} (${columnSQL});`,
          [],
          () => { asyncGet().then(() => { resolve(); }); },
          (_, error) => {
            reject(error);
          },
        );
      },
      (error) => { reject(error); },
    );
  });
}

function updateTable(values, valuesString, columns, table, updateColumn) {
  const key = columns.split(' ')[0];
  const columnNames = columns.split(', ').map(columnInit => columnInit.split(' ')[0]);
  const newTable = `new${table}`;
  const create = `create table ${newTable} (${columns});`;
  const fillNewTable = `insert into ${newTable} (${columnNames}) values ${valuesString}`;
  const deleteRows = `delete from ${table} where ${key} in (select ${key} from ${table} except select ${key} from ${newTable});`;
  const insertNewRows = `insert into ${table} select * from ${newTable} where ${key} in (select ${key} from ${newTable} except select ${key} from ${table});`;
  const update = `update ${table} set ${updateColumn}=(select ${newTable}.${updateColumn} from ${newTable} where ${newTable}.${key}=${table}.${key}) where exists (select * from ${newTable} where ${newTable}.${key}=${table}.${key});`;
  const drop = `drop table ${newTable}`;

  return new Promise((resolve) => {
    Database.transaction(
      (tx) => {
        tx.executeSql(
          create,
          [],
          () => {
            tx.executeSql(
              fillNewTable,
              values,
              () => {
                tx.executeSql(
                  deleteRows,
                  [],
                  () => {
                    tx.executeSql(
                      insertNewRows,
                      [],
                      () => {
                        tx.executeSql(
                          update,
                          [],
                          () => {
                            tx.executeSql(
                              drop,
                              [],
                              () => { console.log(table); resolve(); },
                              (_, error) => {
                                console.log(`error dropping table ${newTable}`);
                                console.log(error);
                              },
                            );
                          },
                          (_, error) => {
                            console.log(`error updating ${table} column ${updateColumn} with new values`);
                            console.log(error);
                            tx.executeSql(`drop table ${newTable};`);
                          },
                        );
                      },
                      (_, error) => {
                        console.log(`error inserting new rows into ${table}`);
                        console.log(error);
                        tx.executeSql(`drop table ${newTable};`);
                      },
                    );
                  },
                  (_, error) => {
                    console.log(`error removing rows from ${table}`);
                    console.log(error);
                    tx.executeSql(`drop table ${newTable};`);
                  },
                );
              },
              (_, error) => {
                console.log(`error inserting values into ${newTable}`);
                console.log(error);
                tx.executeSql(`drop table ${newTable};`);
              },
            );
          },
          (_, error) => { console.log(error); },
        );
      },
      (error) => { console.log(error); },
      null,
    );
  });
}

export const getEvents = async () => {
  fetch('https://www.googleapis.com/calendar/v3/calendars/webmaster@ssa-aes.com/events?key=AIzaSyATBO9paq_ByUOqtlj6TybfoV0WMdWk5gI&reverse_order=true')
    .then(res => res.json())
    .then((res) => {
      const values = [];
      let valuesString = '';

      res.items.forEach((item) => {
        values.push(item.id, 0, JSON.stringify(item));
        valuesString += `${valuesString === '' ? '' : ', '}(?, ?, ?)`;
      });

      return updateTable(
        values,
        valuesString,
        'id text primary key not null, saved int, event text',
        'events',
        'event',
      );
    });
};

const getFAQ = () => (
  API.get('faqAPI', '/faq')
    .then((res) => {
      const values = [];
      let valuesString = '';

      res.data.forEach((row) => {
        valuesString += `${valuesString === '' ? '' : ', '}(?, ?, ?)`;
        values.push(row.question, row.answer, row.language);
      });

      return updateTable(
        values,
        valuesString,
        'question text primary key not null, answer text, language text',
        'faq',
        'answer',
      );
    })
);

const getSocialMedia = () => (
  API.get('socialMediaAPI', '/socialmedia')
    .then((res) => {
      const values = [];
      let valuesString = '';

      res.data.forEach((row) => {
        valuesString += `${valuesString === '' ? '' : ', '}(?, ?)`;
        values.push(row.name, row.value);
      });

      return updateTable(
        values,
        valuesString,
        'name text primary key not null, value text',
        'socialmedia',
        'value',
      );
    })
);

export async function rebuildLocalDB() {
  const eventsPromise = createTable(
    'events',
    ['id', 'saved', 'event'],
    ['text', 'int', 'text'],
    getEvents,
  );

  const credentialsPromise = createTable(
    'credentials',
    ['id', 'pushToken'],
    ['int', 'text'],
    () => new Promise((resolve) => { resolve(); }),
  );

  const faqPromise = createTable(
    'faq',
    ['question', 'answer', 'language'],
    ['text', 'text', 'text'],
    getFAQ,
  );

  const socialMediaPromise = createTable(
    'socialmedia',
    ['name', 'value'],
    ['text', 'text'],
    getSocialMedia,
  );

  return Promise.all([eventsPromise, credentialsPromise, faqPromise, socialMediaPromise]);
}
