import React from 'react';
import {
  Text,
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';
import Constants from './components/Constants';

const { styles } = Constants;
const { month } = Constants;

export class Event extends React.Component {
  constructor() {
    super();
    this.eventOverview = '';
  }

  loadEventOverview() {
    const { event } = this.props;

    const start = new Date(event.start.dateTime);
    const end = new Date(event.end.dateTime);
    let hours = start.getHours();
    let ifMidnight = hours === 0 ? 12 : hours;
    let minutes = start.getMinutes() > 0 ? `:${start.getMinutes()}` : '';
    let period = hours > 11 ? 'p.m.' : 'a.m.';
    hours = hours > 12 ? hours - 12 : ifMidnight;

    let startTime = `${hours}${minutes} ${period}`;

    hours = end.getHours();
    ifMidnight = hours === 0 ? 12 : hours;
    minutes = end.getMinutes() > 0 ? `:${end.getMinutes()}` : '';
    period = hours > 11 ? 'p.m.' : 'a.m.';
    hours = hours > 12 ? hours - 12 : ifMidnight;

    let endTime = `${hours}${minutes} ${period}`;

    const startYear = start.getFullYear();
    const startMonth = start.getMonth();
    const startDate = start.getDate();
    const endYear = end.getFullYear();
    const endMonth = end.getMonth();
    const endDate = end.getDate();

    if ((startYear === endYear) && ((startMonth !== endMonth) || (startDate !== endDate))) {
      startTime = `${month[startMonth]} ${startDate}, ${startTime}`;
      endTime = `${month[endMonth]} ${endDate}, ${endTime}`;
    } else if (start.getFullYear() !== end.getFullYear()) {
      startTime = `${month[startMonth]} ${startDate} ${startYear}, ${startTime}`;
      endTime = `${month[endMonth]} ${endDate} ${endYear}, ${endTime}`;
    }

    this.eventOverview = `${startTime} - ${endTime}${event.location ? ` at ${(event.location.split(','))[0]}` : ''}`;
  }

  render() {
    const { navigation, event, events } = this.props;

    this.loadEventOverview();
    return (
      <TouchableWithoutFeedback onPress={() => navigation.navigate('EventDetails', { currentEvent: event, events, returnRoute: navigation.state.routeName })}>
        <View style={{
          flexDirection: 'column', justifyContent: 'center', alignItems: 'flex-start', backgroundColor: Constants.ssaBlue, margin: 5,
        }}
        >
          <Text style={{ textAlign: 'left', margin: 5 }}>
            <Text style={styles.eventName}>{`${event.summary}\n`}</Text>
            <Text style={styles.eventOverview}>{ this.eventOverview }</Text>
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

Event.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    state: PropTypes.shape({
      routeName: PropTypes.string,
    }),
  }).isRequired,
  event: PropTypes.shape({
    location: PropTypes.string,
    start: PropTypes.shape({
      dateTime: PropTypes.string.isRequired,
    }).isRequired,
    end: PropTypes.shape({
      dateTime: PropTypes.string.isRequired,
    }).isRequired,
    summary: PropTypes.string.isRequired,
  }).isRequired,
  events: PropTypes.shape({}).isRequired,
};
