import { StyleSheet } from 'react-native';

const ssaBlue = '#5bb7e5';

const Constants = {
  ssaBlue,
  powerRed: '#d64655',
  greyedOut: '#dddddd',
  month: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  day: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
  styles: StyleSheet.create({
    background: {
      flex: 1,
      justifyContent: 'flex-start',
      backgroundColor: 'white',
    },
    content: {
      flex: 1,
      justifyContent: 'flex-start',
      margin: 20,
    },
    header: {
      height: 75,
      backgroundColor: 'white',
      flexDirection: 'row',
      justifyContent: 'space-evenly',
      alignItems: 'center',
    },
    headerText: {
      fontSize: 20,
      color: ssaBlue,
      fontFamily: 'bebas-neue-regular',
      textAlign: 'center',
    },
    headerElement: {
      width: '33%',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
    },
    scheduleHeader: {
      width: '50%',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
    },
    title: {
      fontSize: 50,
      color: '#333',
      fontFamily: 'bebas-neue-regular',
    },
    subtitle: {
      alignSelf: 'flex-start',
      fontSize: 30,
      color: '#333',
      fontFamily: 'avenir-light',
    },
    body: {
      margin: 10,
      fontSize: 20,
      color: '#333',
      fontFamily: 'avenir-light',
    },
    question: {
      fontSize: 20,
      color: '#333',
      fontFamily: 'avenir-medium',
    },
    emphasis: {
      color: ssaBlue,
      fontFamily: 'avenir-medium',
    },
    eventName: {
      fontSize: 12,
      color: 'white',
      fontFamily: 'avenir-medium',
    },
    eventOverview: {
      fontSize: 12,
      color: 'white',
      fontFamily: 'avenir-light',
    },
    calendarDate: {
      fontSize: 20,
      color: '#555',
      fontFamily: 'avenir-medium',
    },
  }),
};

export default Constants;
