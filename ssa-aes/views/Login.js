import React from 'react';
import {
  View,
  Animated,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  StatusBar,
} from 'react-native';
import {
  Permissions,
  Notifications,
} from 'expo';
import PropTypes from 'prop-types';
import Svg, { Ellipse, G } from 'react-native-svg';
import { API } from 'aws-amplify';

import Constants from './components/Constants';
import { Database } from './components/Database';

const { styles, ssaBlue, powerRed } = Constants;
const AnimatedEllipse = Animated.createAnimatedComponent(Ellipse);
const AnimatedSvg = Animated.createAnimatedComponent(Svg);
const AnimatedTextInput = Animated.createAnimatedComponent(TextInput);
// const AnimatedTouchableOpacity = Animated.createAnimatedComponent(TouchableOpacity);

function updateLocalCredTable(pushToken) {
  return new Promise((resolve, reject) => {
    Database.transaction(
      (tx) => {
        tx.executeSql(
          'select * from credentials',
          [],
          (_, { rows: { _array } }) => {
            if (_array.length === 0) {
              tx.executeSql(
                'insert into credentials (id, pushToken) values (1, ?)',
                [pushToken],
                () => { resolve(pushToken); },
                (res, error) => { reject(error); },
              );
            } else {
              tx.executeSql(
                'update credentials set pushToken=? where id=1',
                [pushToken],
                () => { resolve(pushToken); },
                (res, error) => { reject(error); },
              );
            }
          },
        );
      },
    );
  });
}

function registerForPushNotificationsAsync() {
  return Permissions.getAsync(Permissions.NOTIFICATIONS)
    .then(({ status }) => {
      if (status !== 'granted') {
        return Permissions.askAsync(Permissions.NOTIFICATIONS);
      }
      return { status };
    })
    .then(({ status }) => {
      if (status !== 'granted') {
        throw new Error('Notification permission not granted');
      }
      return Notifications.getExpoPushTokenAsync();
    })
    .then(token => updateLocalCredTable(token))
    .catch(() => null);
}

export class LoginScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      atomAnim1: new Animated.Value(0),
      atomAnim2: new Animated.Value(775),
      switchFormAnim: new Animated.Value(1),
      fadeAnim: new Animated.Value(0),
      firstName: '',
      lastName: '',
      email: '',
      error: null,
      signUp: true,
    };

    this.emailInput = React.createRef();
    this.firstNameInput = React.createRef();
    this.lastNameInput = React.createRef();
  }

  componentDidMount() {
    const { atomAnim1, atomAnim2, fadeAnim } = this.state;

    Keyboard.addListener('keyboardDidHide', this.emailInput.current._component.blur);
    Keyboard.addListener('keyboardDidHide', this.firstNameInput.current._component.blur);
    Keyboard.addListener('keyboardDidHide', this.lastNameInput.current._component.blur);
    Animated.loop(
      Animated.timing(
        atomAnim1,
        {
          toValue: 950,
          duration: 2000,
        },
      ),
      {
        iterations: -1,
      },
    ).start();
    Animated.loop(
      Animated.timing(
        atomAnim2,
        {
          toValue: 1725,
          duration: 2000,
        },
      ),
      {
        iterations: -1,
      },
    ).start();
    Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: 1000,
      },
    ).start();
  }

  login = async () => {
    const {
      signUp,
      firstName,
      lastName,
      email,
    } = this.state;

    const { navigation } = this.props;

    Keyboard.dismiss();

    if ((signUp && (firstName === '' || lastName === '')) || email === '') {
      this.setState({
        error: 'All fields must be filled out.',
      });
      return;
    }

    if (!email.endsWith('@uottawa.ca')) {
      this.setState({
        error: 'Invalid email inputted.',
      });
      return;
    }

    const dbResponse = await API.get('credAPI', `/credentials/object/${email}`);
    const token = await registerForPushNotificationsAsync();

    if (signUp) {
      if (Object.keys(dbResponse).length > 0) {
        this.setState({
          error: 'This email has already been used to create an account.',
        });
        return;
      }
      API.post(
        'credAPI',
        '/credentials',
        {
          body: {
            email,
            firstName,
            lastName,
            pushToken: token,
          },
        },
      );
    } else if (Object.keys(dbResponse).length === 0) {
      this.setState({
        error: 'No account with this email has been found.',
      });
      return;
    }

    navigation.navigate('App');
  }

  switchForm = () => {
    const { switchFormAnim, signUp } = this.state;

    Animated.timing(
      switchFormAnim,
      {
        toValue: signUp ? 0 : 1,
        duration: 1000,
      },
    ).start();

    this.setState(prevState => ({
      signUp: !prevState.signUp,
      error: '',
    }));
  }

  render() {
    const {
      atomAnim1,
      atomAnim2,
      switchFormAnim,
      fadeAnim,
      signUp,
      error,
    } = this.state;

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: ssaBlue,
          }}
        >
          <StatusBar barStyle="light-content" backgroundColor={ssaBlue} />
          <AnimatedSvg height="250" width="250">
            <G scale="0.5">
              <AnimatedEllipse
                cx="239.275888"
                cy="224.642723"
                fill="none"
                rx="75.206059"
                ry="208.474597"
                stroke="#ffffff"
                strokeWidth="15"
                transform="rotate(33 239.276 224.643)"
                strokeLinecap="round"
                strokeDasharray="600, 350"
                strokeDashoffset={atomAnim2}
              />
              <AnimatedEllipse
                cx="242.94001"
                cy="227.744106"
                fill="none"
                rx="76.212462"
                ry="207.703417"
                stroke="#ffffff"
                strokeWidth="15"
                transform="rotate(-26.5 242.94 227.744)"
                strokeLinecap="round"
                strokeDasharray="775, 175"
                strokeDashoffset={atomAnim1}
              />
              <AnimatedEllipse
                cx="244.075888"
                cy="224.642722"
                fill="none"
                rx="75.206059"
                ry="208.474597"
                stroke="#ffffff"
                strokeWidth="15"
                transform="rotate(93 244.076 224.643)"
                strokeLinecap="round"
                strokeDasharray="600, 350"
                strokeDashoffset={atomAnim2}
              />
              <Ellipse
                cx="241.200002"
                cy="221.200002"
                fill="#ffffff"
                rx="25.599999"
                ry="25.599999"
              />
            </G>
          </AnimatedSvg>
          <Animated.View
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'white',
              height: switchFormAnim.interpolate({
                inputRange: [0, 1],
                outputRange: [0, 40],
              }),
              width: '75%',
              borderRadius: 10,
              opacity: switchFormAnim,
            }}
          >
            <AnimatedTextInput
              ref={this.firstNameInput}
              style={[
                styles.body,
                {
                  opacity: fadeAnim,
                  textAlign: 'center',
                },
              ]}
              clearTextOnFocus
              placeholder="first name"
              onChangeText={text => this.setState({ firstName: text })}
            />
          </Animated.View>
          <Animated.View
            style={{
              height: switchFormAnim.interpolate({
                inputRange: [0, 1],
                outputRange: [0, 10],
              }),
            }}
          >
          </Animated.View>
          <Animated.View
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'white',
              height: switchFormAnim.interpolate({
                inputRange: [0, 1],
                outputRange: [0, 40],
              }),
              width: '75%',
              borderRadius: 10,
              opacity: switchFormAnim,
            }}
          >
            <AnimatedTextInput
              ref={this.lastNameInput}
              style={[
                styles.body,
                {
                  opacity: fadeAnim,
                  textAlign: 'center',
                },
              ]}
              clearTextOnFocus
              placeholder="last name"
              onChangeText={text => this.setState({ lastName: text })}
            />
          </Animated.View>
          <Animated.View
            style={{
              height: switchFormAnim.interpolate({
                inputRange: [0, 1],
                outputRange: [0, 10],
              }),
            }}
          >
          </Animated.View>
          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'white',
              height: 40,
              width: '75%',
              borderRadius: 10,
            }}
          >
            <AnimatedTextInput
              ref={this.emailInput}
              style={[
                styles.body,
                {
                  opacity: fadeAnim,
                  textAlign: 'center',
                },
              ]}
              clearTextOnFocus
              placeholder="someone@uottawa.ca"
              onChangeText={text => this.setState({ email: text })}
            />
          </View>
          <View
            style={{
              height: 10,
            }}
          />
          <TouchableOpacity
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: powerRed,
              height: 40,
              width: 100,
              borderRadius: 10,
            }}
            hitSlop={{
              top: 5, bottom: 5, right: 5, left: 5,
            }}
            onPress={this.login}
          >
            <Animated.Text
              style={[
                styles.headerText,
                {
                  color: 'white',
                  opacity: fadeAnim,
                },
              ]}
              suppressHighlighting
            >
              { signUp ? 'SIGN UP' : 'LOG IN' }
            </Animated.Text>
          </TouchableOpacity>
          <View
            style={{
              height: 10,
            }}
          />
          <TouchableOpacity
            style={{ height: 30 }}
            hitSlop={{
              top: 5, bottom: 5, right: 5, left: 5,
            }}
            onPress={this.switchForm}
          >
            <Animated.Text
              style={[
                styles.calendarDate,
                {
                  color: 'white',
                  opacity: fadeAnim,
                },
              ]}
              suppressHighlight
            >
              { signUp ? 'log into an existing account' : 'sign up for an account' }
            </Animated.Text>
          </TouchableOpacity>
          <View
            style={{
              height: 10,
            }}
          />
          <Animated.Text
            style={[
              styles.calendarDate,
              {
                color: powerRed,
                height: 60,
                width: '75%',
                textAlign: 'center',
                textAlignVertical: 'center',
              },
            ]}
          >
            { error }
          </Animated.Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

LoginScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};
